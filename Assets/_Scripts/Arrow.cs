﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour
{
    // ********************************************************************************************

    // ********************************************************************************************
    [Header("Components")]
    public AudioClip sound;

    [Header("Properties")]
    public float speed;

    // ********************************************************************************************
	void Start ()
    {
        GameData.instance.PlaySound(this.sound);
	}

    // ********************************************************************************************
	void Update ()
    {
        this.transform.Translate(Vector3.up * this.speed * Time.deltaTime);
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Monster"))
            Game.instance.HitMonster();

        if (other.gameObject.CompareTag("Hero") == false)
            Destroy(this.gameObject);
    }

    // ********************************************************************************************

    // ********************************************************************************************

    // ********************************************************************************************

}
