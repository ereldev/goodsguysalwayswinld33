﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BestScores
{
    // ********************************************************************************************
    private const string KEY = "BestScores";
    private const int MAX_STORED_SCORE = 5;

    // ********************************************************************************************
    public List<int> Scores
    {
        get;
        set;
    }

    // ********************************************************************************************
    public BestScores()
    {

    }

    // ********************************************************************************************
    public void Load()
    {
        this.Scores = new List<int>();
        string str = PlayerPrefs.GetString(KEY, "");
        string[] scores = str.Split(';');

        foreach (string score in scores)
        {
            if (score == "")
                continue;

            this.Scores.Add(int.Parse(score));
        }
    }

    public void AddScore(int score)
    {
        this.Scores.Add(score);
        this.Scores.Sort();
        this.Scores.Reverse();

        if (this.Scores.Count > MAX_STORED_SCORE)
            this.Scores = this.Scores.GetRange(0, MAX_STORED_SCORE);
    }

    public void Save()
    {
        string str = "";

        for(int i=0;i<this.Scores.Count;i++)
        {
            str += "" + this.Scores[i];

            if (i != this.Scores.Count - 1)
                str += ";";
        }

        PlayerPrefs.SetString(KEY, str);
        PlayerPrefs.Save();
    }

    // ********************************************************************************************

    // ********************************************************************************************

    // ********************************************************************************************

}
