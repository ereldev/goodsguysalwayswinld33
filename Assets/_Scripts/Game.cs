﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour
{
    // ********************************************************************************************

    // ********************************************************************************************
    public static Game instance;

    [Header("Components")]
    public List<GameObject> projectiles;
    public GameObject heroPrefab;
    public GameObject arrowPrefab;
    public GameObject oilBucketPrefab;

    [Header("Properties")]
    public float spawnFreqMax;
    public float spawnFreqMin;
    public float spawnFreqVariation;
    public float throwFreq;

    public int Health
    {
        get;
        set;
    }
    public int Score
    {
        get;
        set;
    }

    public float spawnFreq;

    // ********************************************************************************************
    void Awake()
    {
        instance = this;
    }

    // ********************************************************************************************
    void Start()
    {
        this.Health = 3;
        this.Score = 0;

        this.spawnFreq = this.spawnFreqMax;

        this.SetProjectile();

        StartCoroutine("SpawnHeroes");
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.S))
            GameUI.instance.OnOilBucketClick();
        else if (Input.GetKeyUp(KeyCode.D))
            GameUI.instance.OnFlamethrowerClick();
    }

    IEnumerator SpawnHeroes()
    {
        while(true)
        {
            GameObject hero = (GameObject) Instantiate(this.heroPrefab);

            float x = Random.Range(-2f, 2f);
            hero.transform.position = new Vector3(x, -6f, 0f);

            yield return new WaitForSeconds(this.spawnFreq);
        }
    }

    IEnumerator ThrowProjectile()
    {
        GameObject projectile = Monster.instance.GetProjectile();

        projectile.transform.SetParent(null);
        Vector3 position = Monster.instance.transform.position;
        position.y -= 1f;
        projectile.transform.position = position;

        projectile.GetComponent<Projectile>().Falling = true;
        Monster.instance.SetProjectile(null);

        yield return new WaitForSeconds(this.throwFreq);

        this.SetProjectile();
    }

    // ********************************************************************************************
    public void StartThrowProjectile()
    {
        StartCoroutine("ThrowProjectile");
    }

    public void HitMonster()
    {
        this.Health--;
        if (this.Health < 0)
            this.Health = 0;

        Monster.instance.Hit();
        GameUI.instance.HitMonster();
        
        if (this.Health == 0)
        {
            StopAllCoroutines();
            this.SaveScore();

            GameUI.instance.ShowDeathPanel();
        }
    }

    public void PushMonster()
    {
        if (this.Health == 0)
            return;

        this.Health = 0;

        Monster.instance.Push();

        StopAllCoroutines();
        this.SaveScore();

        GameUI.instance.ShowDeathPanel();
    }

    public void IncreaseScore()
    {
        this.Score++;

        // Increase Heroes spawn speed
        if (this.Score % 10 == 0 && this.spawnFreq > this.spawnFreqMin)
            this.spawnFreq -= this.spawnFreqVariation;

        GameUI.instance.IncreaseScore();
    }

    // ********************************************************************************************
    private void SetProjectile()
    {
        int index = Random.Range(0, this.projectiles.Count);
        GameObject projectile = (GameObject)Instantiate(this.projectiles[index]);

        Monster.instance.SetProjectile(projectile);
    }

    private void SaveScore()
    {
        BestScores bestScores = new BestScores();
        bestScores.Load();
        bestScores.AddScore(this.Score);
        bestScores.Save();
    }

    // ********************************************************************************************

}
