﻿using UnityEngine;
using System.Collections;

public class GameData : MonoBehaviour
{
    // ********************************************************************************************
    public const float SOUND_VOLUME = 0.5f;

    // ********************************************************************************************
    public static GameData instance;

    public float Sounds
    {
        get;
        set;
    }

    private AudioSource source;

    // ********************************************************************************************
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }
    }

    // ********************************************************************************************
    void Start()
    {
        this.Sounds = SOUND_VOLUME;

        this.source = this.GetComponent<AudioSource>();
    }

    // ********************************************************************************************
    public void PlaySound(AudioClip clip)
    {
        this.source.PlayOneShot(clip, GameData.instance.Sounds);
    }

    // ********************************************************************************************

    // ********************************************************************************************

}
