﻿using UnityEngine;
using System.Collections;

public class Hero : MonoBehaviour
{
    // ********************************************************************************************
    private const float CLIMB_OFFSET = 0.4f;

    // ********************************************************************************************
    [Header("Components")]
    public Transform spriteTransform;
    public SpriteRenderer spriteRenderer;
    public Animator animator;
    public AudioClip[] fallingSounds;
    public AudioClip pushSound;

    [Header("Properties")]
    public float climbSpeed;
    public float moveSpeed;
    public float fallSpeed;
    public float bowMinUsage;
    public float bowMaxUsage;

    private float nextBowUsage;
    private bool bowUsed;
    public bool Attacking
    {
        get;
        set;
    }

    // ********************************************************************************************
	void Start ()
    {
        this.nextBowUsage = Random.Range(this.bowMinUsage, this.bowMaxUsage);
        this.bowUsed = (Random.value >= 0.5f);
        this.Attacking = false;

        StartCoroutine("Climb");
	}
	
    // ********************************************************************************************
    IEnumerator Climb()
    {
        while(true)
        {
            if (Game.instance.Health <= 0)
            {
                StartCoroutine("Fall");
                break;
            }

            this.transform.Translate(0f, Time.deltaTime * this.climbSpeed, 0f);
            if (this.transform.position.y >= Monster.instance.transform.position.y)
                break;

            this.nextBowUsage -= Time.deltaTime;
            if(this.nextBowUsage <= 0f && this.bowUsed == false)
            {
                this.bowUsed = true;

                GameObject arrow = (GameObject)Instantiate(Game.instance.arrowPrefab);
                Vector3 position = this.transform.position;
                position.y += 1f;
                arrow.transform.position = position;
            }

            yield return null;
        }

        if (Game.instance.Health > 0)
            StartCoroutine("AttackMonster");
    }

    IEnumerator AttackMonster()
    {
        this.Attacking = true;

        this.transform.Translate(0f, -CLIMB_OFFSET, 0f);
        this.spriteRenderer.sortingOrder = 1;

        while(true)
        {
            if(Game.instance.Health <= 0)
            {
                StartCoroutine("Fall");
                break;
            }

            float move = Time.deltaTime * this.climbSpeed;
            if (this.transform.position.x > Monster.instance.transform.position.x)
            {
                move = -move;

                this.animator.SetInteger("state", 2);
            }
            else
            {
                this.animator.SetInteger("state", 3);
            }

            this.transform.Translate(move, 0f, 0f);

            yield return null;
        }
    }

    IEnumerator Fall()
    {
        int index = Random.Range(0, this.fallingSounds.Length);
        GameData.instance.PlaySound(this.fallingSounds[index]);

        this.animator.SetInteger("state", 1);

        while (true)
        {
            float move = Time.deltaTime * this.fallSpeed;
            this.transform.Translate(0f, move, 0f);

            yield return null;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Void"))
            Destroy(this.gameObject);
        else if(other.gameObject.CompareTag("Arrow"))
        {
            // Do nothing when hit by an arrow from another hero
        }
        else if (other.gameObject.CompareTag("Hero"))
        {
            // Do nothing when collide with an hero
        }
        else if(other.gameObject.CompareTag("Monster") == false)
        {
            StopAllCoroutines();
            StartCoroutine("Fall");

            Game.instance.IncreaseScore();
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Monster") && this.Attacking)
        {
            StopAllCoroutines();

            GameData.instance.PlaySound(this.pushSound);
            Game.instance.PushMonster();
        }
    }

    // ********************************************************************************************

    // ********************************************************************************************

    // ********************************************************************************************

}
