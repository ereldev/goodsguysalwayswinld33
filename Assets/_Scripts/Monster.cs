﻿using UnityEngine;
using System.Collections;

public class Monster : MonoBehaviour
{
    // ********************************************************************************************

    // ********************************************************************************************
    public static Monster instance;

    [Header("Components")]
    public Transform hand;
    public SpriteRenderer spriteRenderer;
    public Sprite monsterWithHandSprite;
    public Sprite monsterIdleSprite;
    public Sprite monsterFallingSprite;
    public AudioClip[] hitSounds;

    [Header("Properties")]
    public float speed;

    private GameObject projectile;
    private Animator animator;

    // ********************************************************************************************
	void Awake ()
    {
        instance = this;
	}

    // ********************************************************************************************
    void Start()
    {
        this.animator = this.GetComponent<Animator>();
    }

	void Update ()
    {
        if (Game.instance.Health <= 0)
            return;

        // Move Monster
	    if(Input.GetAxis("Horizontal") != 0f)
        {
            float move = Input.GetAxis("Horizontal");

            Vector3 position = this.transform.position;
            Vector2 cameraSize = Utils.GetOrthographicCameraSize(Camera.main);
            
            position.x += move * speed * Time.deltaTime;
            if (position.x < -cameraSize.x / 2f)
                position.x = -cameraSize.x / 2f;
            if (position.x > cameraSize.x / 2f)
                position.x = cameraSize.x / 2f;
            
            this.transform.position = position;
        }

        // Throw Projectile
        if(Input.GetKeyUp(KeyCode.Space))
        {
            if (this.projectile == null || Flamethrower.instance.IsActiveSkill())
                return;

            this.spriteRenderer.sprite = this.monsterIdleSprite;

            Game.instance.StartThrowProjectile();
        }
	}

    // ********************************************************************************************
    public void SetProjectile(GameObject projectile)
    {
        this.projectile = projectile;
        if (this.projectile == null)
            return;

        this.projectile.transform.SetParent(this.hand, false);
        this.projectile.transform.position.Set(0f, 0f, 0f);

        this.spriteRenderer.sprite = this.monsterWithHandSprite;
    }

    public void Hit()
    {
        int index = Random.Range(0, this.hitSounds.Length);
        GameData.instance.PlaySound(this.hitSounds[index]);
    }

    public void Push()
    {
        if (this.projectile)
            Destroy(this.projectile);
        this.SetProjectile(null);

        this.spriteRenderer.sprite = this.monsterFallingSprite;
        this.spriteRenderer.sortingOrder = 3;
        
        this.animator.enabled = true;
        this.animator.SetInteger("state", 1);
    }

    // ********************************************************************************************

    // ********************************************************************************************
    public GameObject GetProjectile() { return this.projectile; }

}
