﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour
{
    // ********************************************************************************************

    // ********************************************************************************************
    public static Music instance;

    [Header("Components")]
    public AudioSource source;

    // ********************************************************************************************
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }
    }

    // ********************************************************************************************
	void Start ()
    {
        this.Play();
	}

    // ********************************************************************************************
    public void Play()
    {
        this.source.Play();
    }

    public void Stop()
    {
        this.source.Stop();
    }

    public bool IsPlaying()
    {
        return this.source.isPlaying;
    }

    // ********************************************************************************************

    // ********************************************************************************************

}
