﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    // ********************************************************************************************

    // ********************************************************************************************
    [Header("Components")]
    public AudioClip[] throwSounds;
    public AudioClip[] hitSounds;

    [Header("Properties")]
    public float speed;
    public bool destroyOnHit;

    private bool falling;
    public bool Falling
    {
        get
        {
            return this.falling;
        }
        set
        {
            this.falling = value;

            if (this.projectileCollider)
                this.projectileCollider.enabled = value;

            if (this.falling && this.throwSounds != null && this.throwSounds.Length != 0)
            {
                int index = Random.Range(0, this.throwSounds.Length);
                GameData.instance.PlaySound(this.throwSounds[index]);
            }
        }
    }

    private Collider2D projectileCollider;

    // ********************************************************************************************
	void Start ()
    {
        this.Falling = false;

        this.projectileCollider = this.GetComponent<Collider2D>();
        this.projectileCollider.enabled = false;
	}

    // ********************************************************************************************
	void Update ()
    {
        if (this.Falling == false)
            return;

        this.transform.Translate(Vector3.down * this.speed * Time.deltaTime);
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (this.Falling == false)
            return;

        if (this.destroyOnHit == false && other.gameObject.CompareTag("Void") == false)
            return;

        if (other.gameObject.CompareTag("Void") == false && this.hitSounds != null && this.hitSounds.Length != 0)
        {
            int index = Random.Range(0, this.hitSounds.Length);
            GameData.instance.PlaySound(this.hitSounds[index]);
        }

        Destroy(this.gameObject);
    }

    // ********************************************************************************************

    // ********************************************************************************************

    // ********************************************************************************************

}
