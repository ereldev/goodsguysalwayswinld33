﻿using UnityEngine;
using System.Collections;

public class Flamethrower : MonoBehaviour
{
    // ********************************************************************************************

    // ********************************************************************************************
    public static Flamethrower instance;

    [Header("Components")]
    public GameObject fireballPrefab;
    public Animator spriteAnimator;

    // ********************************************************************************************
    void Awake()
    {
        instance = this;
    }

    // ********************************************************************************************
	void Start ()
    {
	
	}

    // ********************************************************************************************
    public void ActivateSkill()
    {
        this.spriteAnimator.SetInteger("state", 1);
        this.StartCoroutine("ThrowFireballs");
    }

    public bool IsActiveSkill()
    {
        return (this.spriteAnimator.GetInteger("state") != 0);
    }

    // ********************************************************************************************
    IEnumerator ThrowFireballs()
    {
        yield return new WaitForSeconds(0.5f);

        for (int i = 0; i < 12; i++)
        {
            GameObject fireball = (GameObject) Instantiate(this.fireballPrefab);
            
            Vector3 position = this.transform.position;
            position.x += 1f;
            fireball.transform.position = position;

            yield return new WaitForSeconds(0.4f);
        }
    }

    // ********************************************************************************************

}
