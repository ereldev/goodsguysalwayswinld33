﻿using UnityEngine;
using System.Collections;

public class FlamethrowerAnimation : StateMachineBehaviour
{

	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetInteger("state", 0);
	}

}
