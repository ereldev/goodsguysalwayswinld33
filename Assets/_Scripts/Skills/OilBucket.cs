﻿using UnityEngine;
using System.Collections;

public class OilBucket : MonoBehaviour
{
    // ********************************************************************************************
    private const float TIME_BEFORE_OIL_SPAWNING = 0.5f;

    // ********************************************************************************************
    [Header("Components")]
    public GameObject oilProjectile;

    // ********************************************************************************************
	IEnumerator Start ()
    {
        yield return new WaitForSeconds(TIME_BEFORE_OIL_SPAWNING);

        GameObject projectile = (GameObject)Instantiate(this.oilProjectile);
        Vector3 position = this.transform.position;
        position.y -= 1f;
        projectile.transform.position = position;

        yield return null;

        projectile.GetComponent<Projectile>().Falling = true;

        yield return new WaitForSeconds(GameUI.instance.oilBucketFreq - TIME_BEFORE_OIL_SPAWNING);

        Destroy(this.gameObject);
	}

    // ********************************************************************************************
    public static void Spawn()
    {
        GameObject oilBucket = (GameObject)Instantiate(Game.instance.oilBucketPrefab);
        Vector3 position = Monster.instance.transform.position;
        position.y += 0.1f;
        oilBucket.transform.position = position;
    }

    // ********************************************************************************************

    // ********************************************************************************************

    // ********************************************************************************************

}
