﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameUI : MonoBehaviour
{
    // ********************************************************************************************

    // ********************************************************************************************
    public static GameUI instance;

    [Header("Components")]
    public GameObject oilBucketPanel;
    public Button oilBucketButton;
    public GameObject flamethrowerPanel;
    public Button flamethrowerButton;
    public Sprite heartShadow;
    public List<Image> hearts;
    public Text scoreText;
    public Animator deathPanelAnimator;
    public Text finalScoreText;
    public AudioClip bip1;
    public AudioClip bip2;

    [Header("Properties")]
    public float oilBucketFreq;
    public float flamethrowerFreq;

    private float oilBucketCooldown;
    private float flamethrowerCooldown;

    // ********************************************************************************************
    void Awake()
    {
        instance = this;
    }

    // ********************************************************************************************
    void Update()
    {
        // Oil Bucket Cooldown
        if (this.oilBucketCooldown > 0)
        {
            this.oilBucketCooldown -= Time.deltaTime;

            Vector3 scale = this.oilBucketPanel.transform.localScale;
            scale.x = this.oilBucketCooldown / this.oilBucketFreq;
            this.oilBucketPanel.transform.localScale = scale;
        }

        // Flamethrower Cooldown
        if(this.flamethrowerCooldown > 0)
        {
            this.flamethrowerCooldown -= Time.deltaTime;

            Vector3 scale = this.flamethrowerPanel.transform.localScale;
            scale.x = this.flamethrowerCooldown / this.flamethrowerFreq;
            this.flamethrowerPanel.transform.localScale = scale;
        }
    }

    public void OnOilBucketClick()
    {
        if (this.oilBucketCooldown > 0 || Game.instance.Health <= 0)
            return;

        GameData.instance.PlaySound(this.bip1);
        this.oilBucketCooldown = this.oilBucketFreq;
        OilBucket.Spawn();
    }

    public void OnFlamethrowerClick()
    {
        if (this.flamethrowerCooldown > 0 || Game.instance.Health <= 0)
            return;

        GameData.instance.PlaySound(this.bip1);
        this.flamethrowerCooldown = this.flamethrowerFreq;
        Flamethrower.instance.ActivateSkill();
    }

    public void OnPlayAgainCLick()
    {
        GameData.instance.PlaySound(this.bip1);

        Application.LoadLevel("GameScene");
    }

    public void OnMenuCLick()
    {
        GameData.instance.PlaySound(this.bip2);

        Application.LoadLevel("MenuScene");
    }

    // ********************************************************************************************
    public void HitMonster()
    {
        this.hearts[Game.instance.Health].sprite = this.heartShadow;
    }

    public void IncreaseScore()
    {
        this.scoreText.text = "" + Game.instance.Score;
    }

    public void ShowDeathPanel()
    {
        this.finalScoreText.text = "" + Game.instance.Score;
        this.deathPanelAnimator.SetInteger("state", 1);
    }

    // ********************************************************************************************

    // ********************************************************************************************

}
