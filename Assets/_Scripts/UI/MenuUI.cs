﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuUI : MonoBehaviour
{
    // ********************************************************************************************
    private const string SOUND_KEY = "sound";
    private const string MUSIC_KEY = "music";

    // ********************************************************************************************
    [Header("Components")]
    public GameObject bestScoresPanel;
    public Text bestScoresText;
    public GameObject helpPanel;
    public Image soundImage;
    public Image musicImage;
    public Sprite soundOn;
    public Sprite soundOff;
    public Sprite musicOn;
    public Sprite musicOff;
    public AudioClip bip1;
    public AudioClip bip2;

    // ********************************************************************************************
	void Awake ()
    {
	
	}

    // ********************************************************************************************
    void Start()
    {
        // Init Options
        if(PlayerPrefs.GetInt(SOUND_KEY, 1) == 1)
        {
            this.soundImage.sprite = this.soundOn;
            GameData.instance.Sounds = GameData.SOUND_VOLUME;
        }
        else
        {
            this.soundImage.sprite = this.soundOff;
            GameData.instance.Sounds = 0f;
        }

        if (PlayerPrefs.GetInt(MUSIC_KEY, 1) == 1)
        {
            this.musicImage.sprite = this.musicOn;
            Music.instance.Play();
        }
        else
        {
            this.musicImage.sprite = this.musicOff;
            Music.instance.Stop();
        }

        // Load BestScores
        BestScores bestScores = new BestScores();
        bestScores.Load();

        foreach (int score in bestScores.Scores)
            this.bestScoresText.text += score + "\n";
    }

    public void OnPlayClick()
    {
        GameData.instance.PlaySound(this.bip1);
        Application.LoadLevel("GameScene");
    }

    public void OnBestScoreClick()
    {
        GameData.instance.PlaySound(this.bip1);
        this.bestScoresPanel.SetActive(true);
    }

    public void OnCloseBestScoresClick()
    {
        GameData.instance.PlaySound(this.bip2);
        this.bestScoresPanel.SetActive(false);
    }

    public void OnHelpClick()
    {
        GameData.instance.PlaySound(this.bip1);
        this.helpPanel.SetActive(true);
    }

    public void OnCLoseHelpClick()
    {
        GameData.instance.PlaySound(this.bip2);
        this.helpPanel.SetActive(false);
    }

    public void OnSoundCLick()
    {
        GameData.instance.PlaySound(this.bip1);

        if(GameData.instance.Sounds == 0f)
        {
            this.soundImage.sprite = this.soundOn;
            GameData.instance.Sounds = GameData.SOUND_VOLUME;
            PlayerPrefs.SetInt(SOUND_KEY, 1);
        }
        else
        {
            this.soundImage.sprite = this.soundOff;
            GameData.instance.Sounds = 0f;
            PlayerPrefs.SetInt(SOUND_KEY, 0);
        }

        PlayerPrefs.Save();
    }

    public void OnMusicClick()
    {
        GameData.instance.PlaySound(this.bip1);

        if(Music.instance.IsPlaying() == false)
        {
            this.musicImage.sprite = this.musicOn;
            Music.instance.Play();
            PlayerPrefs.SetInt(MUSIC_KEY, 1);
        }
        else
        {
            this.musicImage.sprite = this.musicOff;
            Music.instance.Stop();
            PlayerPrefs.SetInt(MUSIC_KEY, 0);
        }

        PlayerPrefs.Save();
    }

    // ********************************************************************************************

    // ********************************************************************************************

    // ********************************************************************************************

}
