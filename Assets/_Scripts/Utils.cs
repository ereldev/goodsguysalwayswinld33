﻿using UnityEngine;
using System.Collections;

public class Utils
{

    public static Vector2 GetOrthographicCameraSize(Camera camera)
    {
        float height = 2f * camera.orthographicSize;
        float width = height * camera.aspect;

        return new Vector2(width, height);
    }

}
