Game made in 72 hours during Ludum Dare 33 !

The theme was: You are the monster !

In this game you are a monster that have kidnapped a princess in this tower, but heroes from the entire kingdom want to save the lady and they are climbing your tower !

You have to protect yourself by throwing any object around you !

How many Heroes can you defeat before dying or falling ?

You can play the game on itch.io:
[http://ereldev.itch.io/good-guys-always-win-](http://ereldev.itch.io/good-guys-always-win-)